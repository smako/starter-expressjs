import bodyParser from "body-parser";
import cors from "cors";
import errorhandler from "errorhandler";
import express from "express";
import morgan from "morgan";
import router from "./routes";

var isProduction = process.env.NODE_ENV === "production";

// Créer un objet applicatif global
var app = express();

app.use(cors());

// Paramètres par défaut de la configuration express normale
app.use(morgan("dev"));
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());

app.use(require("method-override")());
app.use(express.static(__dirname + "/public"));

if (!isProduction) {
  app.use(errorhandler());
}

app.use(router);

/// catch 404 et transférer vers le gestionnaire d'erreurs
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  res.sendStatus(404).send(err);
  next(err);
});

/// error handlers

// gestionnaire d'erreurs de développement
// imprimera stacktrace
if (!isProduction) {
  app.use(function (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    console.log(err.stack);

    res.status(Number(res.status) || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

// gestionnaire d'erreurs de production
// aucune trace de pile ne fuit vers l'utilisateur
app.use(function (err: any, req: any, res: any) {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

// enfin, démarrons notre serveur....
const server = app.listen(process.env.PORT || 3000, function () {
  const address = server.address();
  const port = typeof address === "string" ? address : address?.port;
  console.log("Listening on port " + port);
});
