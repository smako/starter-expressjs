"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const errorhandler_1 = __importDefault(require("errorhandler"));
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const routes_1 = __importDefault(require("./routes"));
var isProduction = process.env.NODE_ENV === "production";
// Créer un objet applicatif global
var app = (0, express_1.default)();
app.use((0, cors_1.default)());
// Paramètres par défaut de la configuration express normale
app.use((0, morgan_1.default)("dev"));
app.use(body_parser_1.default.urlencoded({
    extended: false,
}));
app.use(body_parser_1.default.json());
app.use(require("method-override")());
app.use(express_1.default.static(__dirname + "/public"));
if (!isProduction) {
    app.use((0, errorhandler_1.default)());
}
app.use(routes_1.default);
/// catch 404 et transférer vers le gestionnaire d'erreurs
app.use(function (req, res, next) {
    var err = new Error("Not Found");
    res.sendStatus(404).send(err);
    next(err);
});
/// error handlers
// gestionnaire d'erreurs de développement
// imprimera stacktrace
if (!isProduction) {
    app.use(function (err, req, res, next) {
        console.log(err.stack);
        res.status(Number(res.status) || 500);
        res.json({
            errors: {
                message: err.message,
                error: err,
            },
        });
    });
}
// gestionnaire d'erreurs de production
// aucune trace de pile ne fuit vers l'utilisateur
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        errors: {
            message: err.message,
            error: {},
        },
    });
});
// enfin, démarrons notre serveur....
var server = app.listen(process.env.PORT || 3000, function () {
    console.log("Listening on port " + server.address().port);
});
